# PHDstudents_outils-numeriques

Wiki pour rassembler les bonnes idées du discord `PHD STUDENTS-@Home`

C'est par ici que ça se passe!
https://gitlab.com/Florentin/phdstudents_outils-numeriques/-/wikis/home

## Guide de contribution
Guide de contribution du Wiki Outils Numériques du groupe `PHD STUDENTS-@Home` aka :kiwi: :computer: :mortar_board: 

### Qui fait quoi?
- Tout le monde peut consulter le Wiki.
- Toute personne avec un compte Gitlab peut relever une erreur ou proposer un ajout/ une amélioration via le *Issues tracker*.
- Seuls les membres du projet peuvent éditer les pages du Wiki (personne avec un compte GitLab et ajouter en tant que *Developer* au projet).

### Quel contenu?
Sur ce Wiki sont acceptés, tous contenus ayant un rapport avec les outils numériques pour la recherche, tous domaines confondus. Ces contenus doivent néanmoins être suffisamment généraux pour intéresser une communauté de chercheurs. Ci-après une liste non exhaustive de contenu :
  - les liens vers les éditeurs d'outils numériques.
  - les liens vers les guides d'installation, d'usages, tutoriels, et exemples de ces outils.
  - un guide spécialement rédigé sur ce Wiki pour l'installation ou le choix d'un outil ou un Workflow.
Le Wiki est lié à un repo qui contient une licence MIT. Si vous souhaitez contribuez à ce Wiki, mais sans adhérer à la licence associée, créer votre contenu ailleurs et demandez à ce que le lien soit ajouté sur ce Wiki.

### Formatage de la page d'accueil
A l'heure actuelle (09/04/2020) la page d'accueil respecte un certain style graphique afin de faciliter la lecture et l'édition du Wiki.

- Les thèmes sont des titres de type h2, suivis d'un émoticône.
- Les sout-thème sont des titres de type h3, en italique.
- Les sous-thèmes sont divisés en deux rubriques:
  - JID : pour les liens de présentation de l'outil
  - JAQ : pour les questions plus spécifiques
- Notez le nombre de saut à ligne entre les différentes éléments:
  - 3 entre thèmes
  - 2 entre sous-thèmes
  - 1 entre JID et JAQ

Voir en [Annexe](#Annexe) le rendu du code ci-dessous :
```Markdown
## Thème 1 :dog:
### *Sous-thème 1*
JID :telescope: :
- lien 1
- lien 2
- lien ...

JAQ :question: :
- lien 1
- lien 2
- lien ...


### *Sous-thème 2*
JID :telescope: :
- lien 1
- lien 2
- lien ...

JAQ :question: :
- lien 1
- lien 2
- lien ...



## Thème 2 :cat:
```

### Pages supplémentaires
Des pages supplémentaires sont créées lorsqu'un sous-thème devient trop encombrant
sur la page d'accueil, ou nécessite un petit guide ou une explication et pas uniquement des liens.

Chaque page supplémentaire est créée selon le chemin `Theme/Sous-theme`.

Les différentes partie de la page ainsi créée, sont accessible par lien+balise 
`Theme/Sous-theme#point-d-interet` depuis n'importe quelle page du Wiki.

Voir la page [Programmation/Python](Programmation/Python) en exemple.

### Licence
```
MIT License

Copyright (c) 2020 PHD STUDENTS-@Home

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

### Annexe
Aperçu du code Markdown de la page d'accueil :

## Thème 1 :dog:
### *Sous-thème 1*
JID :telescope: :
- lien 1
- lien 2
- lien ...

JAQ :question: :
- lien 1
- lien 2
- lien ...


### *Sous-thème 2*
JID :telescope: :
- lien 1
- lien 2
- lien ...

JAQ :question: :
- lien 1
- lien 2
- lien ...



## Thème 2 :cat: